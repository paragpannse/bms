import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;
  constructor(private router: Router, 
    private fb: FormBuilder) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      regId: new FormControl('',[Validators.required, Validators.min(10), Validators.max(10)]),
      username: new FormControl('',[Validators.required]),
      password: new FormControl('',[Validators.required])
    });
  }

  login() {
    console.log(this.loginForm.value)
    this.router.navigate(['/pin'])
  }
}
