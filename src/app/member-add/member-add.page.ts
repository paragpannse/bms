import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-member-add',
  templateUrl: './member-add.page.html',
  styleUrls: ['./member-add.page.scss'],
})
export class MemberAddPage implements OnInit {
  memberForm;
  constructor(private router: Router, 
    private fb: FormBuilder,
    private modalCtrl: ModalController) { }

  ngOnInit() {
    this.memberForm = this.fb.group({
      name: new FormControl('',[Validators.required, Validators.min(4)]),
      age: new FormControl('',[Validators.required]),
      address: new FormControl('',[Validators.required]),
      idProof: new FormControl('',[Validators.required])
    });
  }

  dismiss() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  addMember(){
    console.log(this.memberForm.value)
  }

}
