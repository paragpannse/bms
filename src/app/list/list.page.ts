import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { MemberInfoPage } from '../member-info/member-info.page';
import { MemberAddPage } from '../member-add/member-add.page';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {

  members;
  constructor(public modalController: ModalController) {
  }
  

  ngOnInit() {
    this.members = [
      {
        name : "parag",
        memberId: 12345,
        age: 30,
        joiningDate: "23/11/1989",
        address : "maggerpatta city hadapsar",
        idProof: "CRNPP12334A"
      },
      {
        name : "Mukesh",
        memberId: 12345,
        age: 30,
        joiningDate: "23/11/1989",
        address : "maggerpatta city hadapsar",
        idProof: "CRNPP12334A"
      },
      {
        name : "Yash",
        memberId: 12345,
        age: 30,
        joiningDate: "23/11/1989",
        address : "maggerpatta city hadapsar",
        idProof: "CRNPP12334A"
      },
      {
        name : "Hari",
        memberId: 12345,
        age: 30,
        joiningDate: "23/11/1989",
        address : "maggerpatta city hadapsar",
        idProof: "CRNPP12334A"
      }
    ];
  }
  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }

  // showMemberInfo(memberId) {
  //   const modal = this.modalController.create({
  //     component: MemberInfoPage
  //   });
  // }

  async presentModal(memberId) {
    const modal = await this.modalController.create({
      component: MemberInfoPage,
      componentProps: {
        'firstName': 'Douglas',
        'lastName': 'Adams',
        'middleInitial': 'N'
      }
    });
    return await modal.present();
  }

  async addMembers() {
    const modal = await this.modalController.create({
      component: MemberAddPage,
      componentProps: {
        'firstName': 'Douglas',
        'lastName': 'Adams',
        'middleInitial': 'N'
      }
    });
    return await modal.present();
  }

}
