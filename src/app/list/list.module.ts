import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { ListPage } from './list.page';
import { MemberInfoPage } from '../member-info/member-info.page';
import { MemberAddPage } from '../member-add/member-add.page';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: ListPage
      }
    ])
  ],
  declarations: [ListPage, MemberInfoPage, MemberAddPage],
  entryComponents:[MemberInfoPage, MemberAddPage]
})
export class ListPageModule {}
