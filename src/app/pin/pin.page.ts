import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-pin',
  templateUrl: './pin.page.html',
  styleUrls: ['./pin.page.scss'],
})
export class PinPage implements OnInit {

  pinForm;
  constructor(private router: Router, 
    private fb: FormBuilder) { }

  ngOnInit() {
    this.pinForm = this.fb.group({
      pin: new FormControl('',[Validators.required, Validators.min(4)]),
      rePin: new FormControl('',[Validators.required])
    });
  }

  setPin() {
    console.log(this.pinForm.value);
    this.router.navigate(['/home'])
  }
}
